package text_editor.ver_two;

import javafx.stage.FileChooser;
import javafx.stage.Stage;
import javax.swing.*;
import java.io.*;
import java.util.Scanner;

/**
 * Created by Neko on 17.01.2017.
 */
public class OpenAndSaveClass {

    String stroka=" error  \n";
    String s = " ";

    static Scanner scn;
    private File currentFile;
    private JEditorPane field;
    File f;

    public void open() {

        FileChooser chooser = new FileChooser();
        File choosed = chooser.showOpenDialog(new Stage());
        f = choosed;
        str(choosed);
    }

    public void str(File choosed) {

        FileReader FR = null;
        try {
            FR = new FileReader(choosed);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        BufferedReader BRf = new BufferedReader(FR);
        s = "";
        do
        {
            try {
                stroka=BRf.readLine();
            } catch (IOException e) {
                e.printStackTrace();
            }

            if(stroka!=null) {
                s = s + stroka + "\n";
            }
        }
        while(stroka!=null);
    }

    public void save(String save){
        saveFile(f, save);
    }

    public void saveFile(File file, String contents) {
        BufferedWriter writer = null;
        String filePath = file.getPath();
        if (!filePath.endsWith(".txt")) {
            filePath = filePath + ".txt";
        }

        try {
            writer = new BufferedWriter(new FileWriter(filePath));
            writer.write(contents);
            writer.close();
            this.field.setText(contents);
            this.currentFile = file;
        } catch (Exception var6) {
            ;
        }

    }
}

